<?php

namespace Nuevo\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Document
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Document {

    private static $uploadDir = 'documents/';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=6)
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     */
    private $alt;

    /**
     * @var string
     *
     * @ORM\Column(name="typeDocument", type="string", length=20, nullable=false)
     */
    private $typeDocument;

    /**
     * @Assert\File(maxSize="500000")
     */
    protected $file;

    /////////////////////////
    ///  GETTER & SETTER  ///
    /////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Document
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Document
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return Document
     */
    public function setExtension($extension) {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension() {
        return $this->extension;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return Document
     */
    public function setAlt($alt) {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string 
     */
    public function getAlt() {
        return $this->alt;
    }

    public function setType($type) {
        $this->typeDocument = $type;

        return $this;
    }

    public function getType() {
        return $this->typeDocument;
    }

    public function setFile($file) {
        $this->file = $file;

        return $this;
    }

    public function getFile() {
        return $this->file;
    }

    /////////////////////////
    ///   LYFCYCLE & CO   ///
    /////////////////////////


    protected function getUploadDir() {
        // On retourne le chemin relatif vers l'image pour un navigateur

        return Document::$uploadDir . $this->getType() . "/";
    }

    protected function getUploadRootDir() {
        // On retourne le chemin relatif vers l'image pour notre code PHP
        return __DIR__ . '/../../../../../web/' . $this->getUploadDir();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null != $this->file) {
            $nom = explode(".", $this->file->getClientOriginalName());
            $this->setExtension($this->file->guessExtension());
            $this->setNom($nom[0]);
            $this->setPath($this->getUploadDir());
            if ($this->typeDocument == "image") {
                $this->setAlt($this->getNom());
            }
        } else {
            throw new \Exception("Ce n'est pas un fichier.");
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null == $this->file) {
            throw new \Exception('Problème de fichier à l\'upload');
        }
        $this->file->move($this->getUploadRootDir(), $this->getNom() . '.' . $this->getExtension());
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

}
