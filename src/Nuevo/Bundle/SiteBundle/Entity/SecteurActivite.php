<?php

namespace Nuevo\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecteurActivite
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Nuevo\Bundle\SiteBundle\Entity\SecteurActiviteRepository")
 */
class SecteurActivite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255)
     */
    private $intitule;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * Get contenu
     * 
     * @return String
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set contenu
     * 
     * @param String
     * @return SecteurActivite
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Id
     * @param integer
     * @return SecteurActivite  
     */
     public function setId($id)
     {
        $this->id = $id;
        return $this;
     }

    /**
     * Set intitule
     *
     * @param string $intitule
     * @return SecteurActivite
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string 
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

}
