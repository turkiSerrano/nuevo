<?php

namespace Nuevo\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecrutementAdmin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Nuevo\Bundle\SiteBundle\Entity\RecrutementAdminRepository")
 */
class RecrutementAdmin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string 
     * 
     * @ORM\Column(name="titreParagraphe1", type="string") 
     */
    private $titreParagraphe1;
  
    /**
     * @var string
     *
     * @ORM\Column(name="paragraphe1", type="blob")
     */
    private $paragraphe1;

    /**
     * @var string
     *
     * @ORM\Column(name="paragraphe2", type="blob")
     */
    private $paragraphe2;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paragraphe1
     *
     * @param string $paragraphe1
     * @return RecrutementAdmin
     */
    public function setParagraphe1($paragraphe1)
    {
        $this->paragraphe1 = $paragraphe1;

        return $this;
    }

    /**
     * Get paragraphe1
     *
     * @return string 
     */
    public function getParagraphe1()
    {
        return $this->paragraphe1;
    }

    /**
     * Set paragraphe2
     *
     * @param string $paragraphe2
     * @return RecrutementAdmin
     */
    public function setParagraphe2($paragraphe2)
    {
        $this->paragraphe2 = $paragraphe2;

        return $this;
    }

    /**
     * Get paragraphe2
     *
     * @return string 
     */
    public function getParagraphe2()
    {
        return $this->paragraphe2;
    }
    
    /**
     * Get titreParagraphe1
     * 
     * @return string
     */
    public function getTitreParagraphe1()
    {
        return $this->titreParagraphe1;
    }
    
    /**
     * Set titreParagraphe1
     * 
     * @param string $titreParagraphe1
     * @return RecrutementAdmin
     */
    public function setTitreParagraphe1($titreParagraphe1)
    {
        $this->titreParagraphe1 = $titreParagraphe1;
        
        return $this;
    }
}
