<?php

namespace Nuevo\Bundle\SiteBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ActualiteRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ActualiteRepository extends EntityRepository
{

    private $nbParPage;
    
    public function ActualiteRepository($nbParPage)
    {
        $this->nbParPage = $nbParPage;
    }

    /**
     * fonction qui creer un reportage en 
     * base de données 
     */
    function insererActu($actu)
    {
            $em = $this->getEntityManager();
            $em->persist($actu);
            $em->flush();
    }

    /**
     * Renvoi les actualites les plus récentes classés par date
     * Le nombre d'actualties renvoyés est limite à 8
     * @todo terminer le  parametrage du nbr d'actus remonter
     * @return Array le tableau des actualites
     */
    function getLastActus()
    {
            $lastActus = $this->findBy(
                    array(),
                    array('dateCreation' => 'desc'), 
                    8
            );
            return  $lastActus;
    }

    /**
     * Fonction utilisez par le menu 
     * @param type $page
     * @return type
     */
    public function getPageActu($page)
    {
            $query = $this->getEntityManager()
        ->createQuery('
            SELECT a FROM NuevoSiteBundle:Actualite a 
            ORDER BY a.dateCreation DESC
            ')
            ->setMaxResults(4);
//      		->setParameters( array('minIndex' => $minIndex, 'maxIndex' => $maxIndex ));

        return $query->getResult();
    }

    function getPage($page,$nbParPage)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT a '
            . 'FROM NuevoSiteBundle:Actualite a '
            . 'ORDER BY a.dateCreation'    
        );
        $query->setFirstResult( ($page - 1) * $nbParPage);
        $query->setMaxResults($nbParPage);
        
        return $query->getResult();
    }
	

}
