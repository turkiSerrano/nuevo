<?php

namespace Nuevo\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TypeFormation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Nuevo\Bundle\SiteBundle\Entity\TypeFormationRepository")
 */
class TypeFormation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $intitule;
    
    /**
     *
     * @var string
     * @ORM\Column(name="url", type="string", length=255) 
     */
    private $url;
    
    /**
    *
     * @ORM\ManyToMany(targetEntity="Nuevo\Bundle\SiteBundle\Entity\SecteurActivite", cascade={"persist"})
    *  @var array
    *
    **/
    private $secteurs;

    public function __construct(){
        $this->secteurs = new  ArrayCollection();
    }

    /**
    * Get secteurs
    * 
    * @return array
    */    
    public function getSecteurs()
    {
        return $this->secteurs;
    }

    /**
    * Set secteurs
    *
    * @param array $secteurs
    * @return TypeFormation
    */
    public function setSecteurs(ArrayCollection $secteurs)
    {
        $this->secteurs = $secteurs;

        return $this;
    }

    /**
     * Get url
     * 
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set url
     * 
     * @return TypeFormation
     */
    public function setUrl($url)
    {
        $this->url = $url;
        
        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     * @return TypeFormation
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string 
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

}
