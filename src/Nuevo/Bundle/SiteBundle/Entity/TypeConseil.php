<?php

namespace Nuevo\Bundle\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * TypeConseil
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Nuevo\Bundle\SiteBundle\Entity\TypeConseilRepository")
 */
class TypeConseil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255)
     */
    private $intitule;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Nuevo\Bundle\SiteBundle\Entity\SecteurActivite", cascade={"persist"})
     */
    private $secteurs;

    public function __construct(){
        $this->secteurs = new  ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     * @return TypeConseil
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string 
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return TypeConseil
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set secteurs
     *
     * @param array $secteurs
     * @return TypeConseil
     */
    public function setSecteurs($secteurs)
    {
        $this->secteurs = $secteurs;

        return $this;
    }

    /**
     * Get secteurs
     *
     * @return array 
     */
    public function getSecteurs()
    {
        return $this->secteurs;
    }
}
