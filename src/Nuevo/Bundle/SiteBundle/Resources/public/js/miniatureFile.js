window.addEventListener('load', init, false);

function init() {
    var inputCV = document.querySelector('#nuevo_recrutement_form_cv');
    var inputMotiv = document.querySelector('#nuevo_recrutement_form_motivation');
    inputCV.addEventListener('change', chargementImage, false);
    inputMotiv.addEventListener('change', chargementImage, false);
}
function chargementImage(e) {
    var files = e.target.files;
    var output = (e.target.nextSibling);
    var f = files[0];
    var lecteur = new FileReader();
    lecteur.addEventListener('load', function (file) {
        return function (e) {
            var type;
            switch (e.type) {
                case 'application/pdf':
                    type = 'pdf';
                    break;
                case 'application/vnd.ms-excel':
                case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                    type = 'excel';
                    break;
                case 'application/msword':
                case'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                    type = 'word';
                    break;
                default:
                    type = 'niet';
            }
            output.innerHTML = ['<img src="http://localhost:8888/Nuevo/web/bundles/nuevosite/images/icones/' + type + '.png" title="', e.name, '"/><br>' + e.name].join('');
        }(f);

    });
    lecteur.readAsDataURL(f);
}