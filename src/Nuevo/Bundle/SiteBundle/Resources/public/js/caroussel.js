/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

  

jQuery(function($){
    
    setInterval(function(){ 
        caroussel();
    }, 3000);
    
    //sens de deroulement des photos 
    //true == de haut en 
    var dir_forward = true;

    function forward()
    {
        $('.screen:visible').slideUp('slow');
        $('.screen:visible').next().slideDown('slow');    
    }
    
    function reverse()
    {
        $('.screen:visible').slideUp('slow');
        $('.screen:visible').prev().slideDown('slow');
    }
    
    function goForward()
    {
        if($('.screen:visible').next().length === 0 ){
            dir_forward = false;
            return false;
        }
        return  true;
    }
    
    function goBackward()
    {
        if($('.screen:visible').prev().length === 0 ){
            dir_forward = true;
            return false;
        }
        return  true;
    }
    
    function caroussel(){
        
        if(goForward() && dir_forward){
            forward();
        }else if(goBackward()){
            reverse();
        }
        
    }
    
});