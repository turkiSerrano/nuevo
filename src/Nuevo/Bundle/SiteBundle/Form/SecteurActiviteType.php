<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
/**
 * Description of SecteurActiviteType
 *
 * @author tontonascii
 */
class SecteurActiviteType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('intitule','text',  array(
                'attr' => array("placeholder" => "entrez le nom du secteur d'activité")))
            ->add('contenu','textarea', array(
                'trim' => true, 'label' => 'contenu',
                'attr' => array( 'placeholder' => "entrer le descriptif", 'class' => 'tinymce')))
            ;
    }
    
    
/**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
            
        $resolver->setDefaults(array(
            'data_class' => 'Nuevo\Bundle\SiteBundle\Entity\SecteurActivite'
        ));
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'nuevo_bundle_sitebundle_secteur_activite';
    }

}
