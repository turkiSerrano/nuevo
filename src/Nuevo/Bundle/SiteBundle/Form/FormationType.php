<?php

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormationType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('titre', 'text')
            ->add('objectifs', 'collection', array(
                'type' => 'text',
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true
            ))
            ->add('programme', 'collection', array(
                'type' => 'text',
                'allow_add' => true,
                'allow_delete' => true
                ))
//            ->add('public')
            ->add('duree', 'time',array(
                'input' => 'datetime',
                'widget' => 'choice'
            ))
            ->add('dateDebut', 'date', array(
                'format' => 'dd MM yyyy'
            ))
            ->add('dateFin', 'date', array(
                'format' => 'dd MM yyyy'
            ))
            ->add('contact', 'text')
            ->add('lieu', 'text')
            ->add('validez', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nuevo\Bundle\SiteBundle\Entity\Formation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nuevo_bundle_sitebundle_formation';
    }
}
