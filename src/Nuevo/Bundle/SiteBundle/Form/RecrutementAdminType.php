<?php

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
/**
 * Description of RecrutementAdmin
 *
 * @author tontonascii
 */
class RecrutementAdminType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add(
            'paragraphe1', 'textarea', array(
            'attr' => array(
                'class' => 'tinymce',
                'data-theme' => 'medium' // simple, advanced, bbcode
            )))
            ->add(
            'paragraphe2', 'textarea', array(
            'attr' => array(
                'class' => 'tinymce',
                'data-theme' => 'medium' // simple, advanced, bbcode
            )))    
            ->add('valider','submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }
    
    public function getName() {
        return 'nuevo_recrutement_admin_form';
    }

}
