<?php

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ActualiteType extends AbstractType{
    
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('titre', 'text', array('trim' => true, 'label' => 'Titre'
            ))
            ->add('sousTitre', 'text', array('trim' => true, 'label' => 'sousTitre'
            ))
            ->add('auteur', 'text', array('trim' => true, 'label' => 'auteur'
            ))
            ->add('dateCreation', 'date', array('label' => 'date'
            ))
            ->add('texte', 'textarea', array(
                'trim' => true, 'label' => 'texte',
                'attr' => array( 'class' => 'tinymce'                    )
                ))
            ->add('valider', 'submit');
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'attr' => ['id' => 'actualiteForm', 'class'=>'col-lg-12']
        ));
    }
    
    public function getName() {
        return "nuevo_actualite_form";
    }

    
}