<?php

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Nuevo\Bundle\SiteBundle\Form\SecteurActiviteType;

/**
 *
 * @author kturki85@gmail.com
 */
class TypeConseilType extends AbstractType 
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('intitule','text')
			->add('secteurs','collection',array(
				'type' => new SecteurActiviteType(),
				'allow_delete' => true,
				'allow_add' => true,
				'prototype' => true
				))
			->add('url','text')
            ->add('valider', 'submit');
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
            
        $resolver->setDefaults(array(
            'data_class' => 'Nuevo\Bundle\SiteBundle\Entity\TypeConseil'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nuevo_bundle_sitebundle_type_conseil';
    }
    

}