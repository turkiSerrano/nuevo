<?php

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Nuevo\Bundle\SiteBundle\Form\SecteurActiviteType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of TypeFormationType
 *
 * @author tontonascii
 */
class TypeFormationType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('secteurs','collection', array(
                'type' => new SecteurActiviteType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true
            ))
            ->add('intitule', 'text', array( 'trim' => 'true' ,'label' => 'intitule'
                ))
            ->add('url', 'text', array(
                'attr' => array("placeholder" => "entrez une url")
            ))
            ->add('valider', 'submit');   
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
            
        $resolver->setDefaults(array(
            'data_class' => 'Nuevo\Bundle\SiteBundle\Entity\TypeFormation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nuevo_bundle_sitebundle_type_formation';
    }
    
}
