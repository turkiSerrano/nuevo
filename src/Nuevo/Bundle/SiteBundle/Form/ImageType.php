<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Nuevo\Bundle\SiteBundle\Form;

/**
 * Description of ImageType
 *
 * @author tontonascii
 */
class ImageType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('photo', 'file');   
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Nuevo\Bundle\SiteBundle\Entity\TypeImage'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'nuevo_bundle_sitebundle_type_image';
    }
}
