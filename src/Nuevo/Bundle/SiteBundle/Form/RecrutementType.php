<?php

namespace Nuevo\Bundle\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Formulaire de recrutement 
 */
class RecrutementType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array('trim' => true, 'label' => 'Nom',
                    'constraints' => array(
                        new Regex(array('pattern' => '/^[a-zA-Z\d][\s\da-zA-Zéêèùàçïöäë]{1,39}$/', "message" => "Le nom doit contenir au minimum 2 caractères. Les caractères spéciaux sont interdit. ")),
                        new NotNull(array("message" => "Ce champ est obligatoire")),
                        new NotEqualTo(array("value" => "undefined", "message" => "Ce champ est obligatoire"))
                    ),
                    'attr' => array('placeholder' => "Insérez votre nom")
                ))
                ->add('prenom', 'text', array('trim' => true, 'label' => 'Prénom',
                    'constraints' => array(
                        new Regex(array('pattern' => '/^[a-zA-Z\d][\s\da-zA-ZZéêèùàçïöäë]{1,39}$/', "message" => "Le prénom doit contenir au minimum 2 caractères. Les caractères spéciaux sont interdit")),
                        new NotNull(array("message" => "Ce champ est obligatoire")),
                        new NotEqualTo(array("value" => "undefined", "message" => "Ce champ est obligatoire"))
                    ),
                    'attr' => array('placeholder' => "Insérez votre prénom")
                ))
                ->add('cv', 'file', array('label' => 'Cv',
                    'constraints' => array(
                        new File(array(
                            'maxSize' => '500k', 'maxSizeMessage' => 'Le fichier ne doit pas dépasser 500 Ko',
                            'mimeTypes' => array(
                                'application/pdf',
                                'application/vnd.ms-excel',
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                'application/msword',
                                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                            'mimeTypesMessage' => 'Seul les .pdf, .doc, .docx, xls et xlsx sont autorisé'))
            )))
                ->add('motivation', 'file', array('required' => false, 'label' => 'Lettre de motivation',
                    "constraints" => array(
                        new File(array(
                            'maxSize' => '500k', 'maxSizeMessage' => 'Le fichier ne doit pas dépasser 500 Ko',
                            'mimeTypes' => array(
                                'application/pdf',
                                'application/msword',
                                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
                            'mimeTypesMessage' => 'Seul les .pdf, .doc et .docx sont autorisé'))
            )))
                ->add('valider', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'attr' => ['id' => 'recrutementForm', 'class' => 'col-lg-12', 'enctype' => 'multipart/form-data']
        ));
    }

    public function getName() {
        return 'nuevo_recrutement_form';
    }

}
