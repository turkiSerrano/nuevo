<?php

namespace Nuevo\Bundle\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nuevo\Bundle\SiteBundle\Entity\Actualite;
use Nuevo\Bundle\SiteBundle\Form\RecrutementType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * [indexAction description]
     * fonction qui renvoie la page d'index du site
     * @return Response 
     */
    public function indexAction()
    {
        $actuRepo = $this->get('nuevo_site.repository.actualite');
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $actus = $actuRepo->getLastActus();
        $categoriesFormation = $repoCatFormation->getCategorieFormation();
        return $this->render('NuevoSiteBundle:Default:index.html.twig', array('actualites' => $actus, 'categories' => $categoriesFormation));
    }
    
    public function presentationAction()
    {
        return $this->render('NuevoSiteBundle::nuevoPresentation.html.twig' );
    }
    
    /**
     * Menu du site
     */
    public function menuAction()
    {
        $repoCatConseil = $this->get('nuevo_site.repository.typeConseil');
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $categories = $repoCatFormation->getCategorieFormation();
        $conseils = $repoCatConseil->findAll();
        
       return $this->render(
            'NuevoSiteBundle:Default:header.html.twig',
            array('categories' => $categories, 'conseils' => $conseils)
        );
    }
    
    public function typeFormationAction($url)
    {
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $categorie = $repoCatFormation->getCategorieByUrl($url);
        
        return $this->render('NuevoSiteBundle::typeFormation.html.twig',
                array('categorie' => $categorie));
    }
    
    public function typeConseilAction($url)
    {
        $repoConseil = $this->get('nuevo_site.repository.typeConseil');
        $categorie = $repoConseil->findByUrl($url);

        return $this->render('NuevoSiteBundle::typeConseil.html.twig',
                array('categorie' => $categorie));
    }

    public function menuCategorieAction()
    {
        
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $categories = $repoCatFormation->getCategorieFormation();
        
        return $this->render('NuevoSiteBundle::menuCategorieFormation.html.twig', 
                array('categories' => $categories));
    }
}
