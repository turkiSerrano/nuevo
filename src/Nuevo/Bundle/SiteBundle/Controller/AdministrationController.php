<?php
namespace Nuevo\Bundle\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nuevo\Bundle\SiteBundle\Form\RecrutementAdminType;
use Nuevo\Bundle\SiteBundle\Form\ActualiteType;
use Symfony\Component\HttpFoundation\Request;
use Nuevo\Bundle\SiteBundle\Entity\Actualite;
use Nuevo\Bundle\SiteBundle\Entity\Formation;
use Nuevo\Bundle\SiteBundle\Entity\SecteurActivite;
use Nuevo\Bundle\SiteBundle\Entity\TypeFormation;
use Nuevo\Bundle\SiteBundle\Entity\TypeConseil;
use Nuevo\Bundle\SiteBundle\Form\FormationType;
use Nuevo\Bundle\SiteBundle\Form\TypeFormationType;
use Nuevo\Bundle\SiteBundle\Form\TypeConseilType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdministrationController
 *
 * @author tontonascii
 */
class AdministrationController extends Controller {
    
    
    public function indexAction()
    {
        return $this->render('NuevoSiteBundle:Admin:index.html.twig' );
    }
    
    public function recrutementAction(Request $request)
    {
        
        $form = $this->get('form.factory')->create(new RecrutementAdminType());
        
        if ($form->handleRequest($request)->isValid()) {
            
            return $this->render('NuevoSiteBundle:Admin:recrutement.html.twig', array('form' => $form->createView()));
        }

        return $this->render('NuevoSiteBundle:Admin:recrutement.html.twig', array('form' => $form->createView()) );
    }
    
    /**
     * 
     * @param Request $request
     * @return Response
     */
    public function actualiteAction(Request $request)
    {
        $actualite = new Actualite();
        $form = $this->get('form.factory')->create(new ActualiteType(),$actualite);
        
        if ($form->handleRequest($request)->isValid()) {
            $actuRepo = $this->get('nuevo_site.repository.actualite');
            $actuRepo->insererActu($actualite);
            var_dump("sauvegarde effectuée");
            return $this->render('NuevoSiteBundle:Admin:actualite.html.twig', array('form' => $form->createView()));
        }

        return $this->render('NuevoSiteBundle:Admin:actualite.html.twig', array('form' => $form->createView()) ); 
    }
    
    /**
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param String intitule secteur activite
     * 
     * @return String response   
     */
    public function formationAction(Request $request, $secteur)
    {
        $formation = new Formation();
        $form = $this->get('form.factory')->create(new FormationType(), $formation);

        if ($form->handleRequest($request)->isValid()) 
        {

            $dateDebut = $form->get('dateDebut')->getData();
            $dateFin = $form->get('dateFin')->getData();
            $datesOk  = ( $dateFin > $dateDebut ); 
            if ($datesOk) {
                $formationRepo = $this->get('nuevo_site.repository.formation');
                $secteur = $this->get('nuevo_site.repository.SecteurActivite')->findByintitule($secteur);
                $formation->setSecteur($secteur[0]);
                $formationRepo->insererFormation($formation);
                return $this->forward('NuevoSiteBundle:Administration:index');
            }
            //retour formulaire si date incoherentes
            return $this->render('NuevoSiteBundle:Admin:formation.html.twig', array('form' => $form->createView()) );
        }
        
        return $this->render('NuevoSiteBundle:Admin:formation.html.twig', array('form' => $form->createView()) );
    }
    
    /**
     * Gere la soumission du fomrulaire pour la creation d'une categorie de formation
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function typeFormationAction(Request $request)
    {
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $typeFormation = new TypeFormation() ;

        $form = $this->get('form.factory')->create(new TypeFormationType(), $typeFormation);
        
        if ($form->handleRequest($request)->isValid()) {

            $repoCatFormation->insererTypeFormation($typeFormation);
            return $this->forward('NuevoSiteBundle:Administration:typeFormationGestion');
        }
        
        return $this->render('NuevoSiteBundle:Admin:typeFormation.html.twig', array('form' => $form->createView()) );
    }
    
    /**
     * Liste les categories de formation 
     * @return Response
     *                                  
     */
    public function typeFormationGestionAction()
    {
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $categories = $repoCatFormation->findAll();

        return $this->render('NuevoSiteBundle:Admin:gestionCategorieFormation.html.twig',
            array("categories" => $categories ));
    }
    
    /**
     * Supprime une categorie passé en parametre
     * @param String 
     * @return Response
     * 
     */
    public function supprimerCategorieFormationAction($url)
    {
        $repoCatFormation = $this->get('nuevo_site.repository.typeFormation');
        $repoCatFormation->supprimerCategorie($url);
        $categories = $repoCatFormation->findAll();

        return $this->render('NuevoSiteBundle:Admin:gestionCategorieFormation.html.twig',
            array("categories" => $categories ));
    }
    
    /**
     * Modifie une categorie
     * @ParamConverter("typeFormation", options={"mapping" : { "url" : "url"}} )
     *
     */
    public function modifierCategorieAction(TypeFormation $typeCategorie, Request $request)
    {
        $form = $this->get('form.factory')->create(new TypeFormationType(), $typeCategorie);
        
        if ($form->handleRequest($request)->isValid()) {

            $this->get('nuevo_site.repository.typeFormation')->insererTypeFormation($typeCategorie);
        }
        
        return $this->render('NuevoSiteBundle:Admin:typeFormation.html.twig',
            array('form' => $form->createView()) );
    }

    /**
     * Renvoi le secteur choisie et les formations associes
     * @ParamConverter("secteur", class="NuevoSiteBundle:SecteurActivite")
     *
     */
    public function modifierSecteurAction(SecteurActivite $secteurActivite)
    {
        $formations = $this->get('nuevo_site.repository.formation')
            ->getFormationsBySecteur($secteurActivite);     

        return $this->render('NuevoSiteBundle:Admin:secteurActivite.html.twig',
            array("secteur" => $secteurActivite, "formations" => $formations));
    }

    /**
     * Renvoie vers le menu conseil gestion des secteurs 
     * d'activite, des fiches conseil
     *
     */
    public function conseilAction()
    {
        return $this->render('NuevoSiteBundle:Admin:gestionConseil.html.twig');
    }


    /**
     * Creation d'une categorie pour la rubrique conseil
     * formulaire de creation
     *
     */
    public function creerCategorieConseilAction(Request $request)
    {
        $repoTypeConseil = $this->get('nuevo_site.repository.typeConseil');
        $conseil = new  TypeConseil();

        $form = $this->get('form.factory')->create(new TypeConseilType(), $conseil);

        if($form->handleRequest($request)->isValid()){
            $repoTypeConseil->creerCategorieConseil($conseil);
        }

        return $this->render('NuevoSiteBundle:Admin:typeConseil.html.twig',
             array('form' => $form->createView() ));

    }
}
