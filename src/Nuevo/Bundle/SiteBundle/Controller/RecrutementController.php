<?php

namespace Nuevo\Bundle\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nuevo\Bundle\SiteBundle\Form\RecrutementType;
use Symfony\Component\HttpFoundation\Request;
use Nuevo\Bundle\SiteBundle\Entity\Candidat;
use Nuevo\Bundle\SiteBundle\Entity\Document;

/**
 * Create view for Recrutement part
 * 
 * Get Formulaire Data from the view and persist the data
 *
 * @author Sébastien Serrano <sebastien.serrano@outlook.fr>
 */
class RecrutementController extends Controller {

    public function recrutementAction(Request $request) {
        $form = $this->get('form.factory')->create(new RecrutementType());

        if ($form->handleRequest($request)->isValid()) {
            $data = $form->getData();

            $cv = $this->makeDoc($data['cv'], 'cv');
            $motivation = $this->makeDoc($data['motivation'], 'motivation');

            $em = $this->getDoctrine()->getManager();

            $em->persist($cv);
            if (null != $motivation) {
                $em->persist($motivation);
                $em->persist($this->createCandidat($form, $cv, $motivation));
            } else {
                $em->persist($this->createCandidat($form, $cv));
            }

            $em->flush();

            return $this->render('NuevoSiteBundle:recrutement:recrutementFormSend.html.twig');
        }

        return $this->render('NuevoSiteBundle:recrutement:recrutement.html.twig', array('form' => $form->createView()));
    }

    private function makeDoc($file, $type) {
        if (null == $file || !isset($file) || empty($file)) {
            return null;
        }

        if ($type !== "cv" && $type !== "motivation" && $type !== "image") {
            throw new \InvalidArgumentException("InvalidArgumentException : The argument 'type' must be 'cv', 'motivation' or 'image'. $type given.");
        }

        $doc = new Document();
        $doc->setFile($file);
        $doc->setType($type);

        return $doc;
    }

    private function createCandidat($form, $cv, $motivation = null) {
        $candidat = new Candidat();
        $data = $form->getData();
        $date = new \DateTime();

        $candidat->setNom($data['nom']);
        $candidat->setPrenom($data['prenom']);
        $candidat->setDate($date);
        $candidat->setCv($cv);
        $candidat->setMotivation($motivation);

        return $candidat;
    }

}
