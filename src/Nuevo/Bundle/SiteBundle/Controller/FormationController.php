<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Nuevo\Bundle\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of FormationController
 *
 * @author tontonascii
 */
class FormationController extends Controller{
    
    /**
     * Renvoie la page d'acceuil de l'onglet formation
     * 
     */
    public function indexAction()
    {
        return $this->render('NuevoSiteBundle::formation.html.twig');
    }
    
}
