<?php

namespace Nuevo\Bundle\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of ActualiteController
 *
 * @author sebastienserrano
 */
class ActualiteController extends Controller {

    
    /**
     * [getActuAction description]
     * methode qui renvoie la page d'actualites
     * @return [type] [description]
     */
    public function getActuAction($page) {

        $page       = (empty($page)) ? 1 : $page;
        $actualites = $this->get('nuevo_site.repository.actualite')->getPage($page,2);

        return $this->render('NuevoSiteBundle::actualites.html.twig', array('actus' => $actualites, 'page' => $page));
    }

    /**
     * Renvoie les 4 dernieres actualites
     * @return [type] [description]
     */
    public function getLastActu() {
        $actuRepo = $this->get('nuevo_site.repository.actualite');
        $actualites = $actuRepo->getLastActus();
        return $actualites;
    }

}
